/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * BARIA - Backup And Restore Installed Apps
 * Copyright (C) 2016  vishnu@easwareapps.com
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.easwareapps.baria.utils;

import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.easwareapps.baria.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;


public class CopyTask extends AsyncTask<Void, PInfo, Void> {

    private ArrayList<PInfo> packages;
    private int totalCount = 0;
    private int current = 0;
    private int error=  0;
    private Context context;
    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;

    public  CopyTask(Context ctx, ArrayList<PInfo> packages) {
        this.packages = packages;
        this.context = ctx;
        for (PInfo pkg: packages) {
            if(pkg.selected) {
                totalCount++;
            }
        }
    }


    @Override
    protected void onPreExecute() {
        //Create Progressbar
        mNotifyManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setContentTitle(context.getString(R.string.copy_started))
                .setContentText(context.getString(R.string.copying, current, totalCount))
                .setSmallIcon(R.mipmap.ic_launcher);
        mNotifyManager.notify(14098, mBuilder.build());
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... voids) {

        saveApps();
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        String errorMsg = "";
        if(error != 0)
            errorMsg = context.getString(R.string.errors, error);

        mBuilder.setContentTitle(context.getString(R.string.copy_finished))
                .setContentText(context.getString(R.string.copyied, current, totalCount) + errorMsg)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                        R.mipmap.ic_copy));
        mBuilder.setProgress(totalCount, current, false);
        mNotifyManager.notify(14098, mBuilder.build());

        super.onPostExecute(aVoid);
    }

    @Override
    protected void onProgressUpdate(PInfo... info) {
        Bitmap bitmap = null;
        Drawable icon = info[0].icon;
        try {

            bitmap = Bitmap.createBitmap(icon.getIntrinsicWidth(), icon.getIntrinsicHeight()
                    , Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            icon.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            icon.draw(canvas);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mBuilder.setProgress(totalCount, current, false);
        mBuilder.setContentTitle(context.getString(R.string.copy_app, info[0].appname))
                .setContentText(context.getString(R.string.copying, current, totalCount));
        if(bitmap != null) {
            mBuilder.setLargeIcon(bitmap);
        }else {
            mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }
        mNotifyManager.notify(14098, mBuilder.build());
        super.onProgressUpdate(info);

    }

    private void saveApps() {

        BariaPref pref = BariaPref.getInstance(context);
        String path = pref.getBackupDir();
        File opdir = new File(path);
        if(! opdir.exists()) {
            if(!opdir.mkdirs()){
                Toast.makeText(context, context.getString(R.string.cant_write), Toast.LENGTH_LONG).show();
                error++;
                return;
            }
        }
        for(PInfo packageInfo: packages){
            if(packageInfo.selected) {
                current++;
                publishProgress(packageInfo);
                copyFile(packageInfo.apk, opdir.getAbsolutePath() + "/" + packageInfo.pname + ".apk");
            }


        }
    }

    private void copyFile(String inputFile, String outputPath) {

        InputStream in;
        OutputStream out;
        try {

            in = new FileInputStream(inputFile);
            out = new FileOutputStream(outputPath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();

            // write the output file (You have now copied the file)
            out.flush();
            out.close();

        }  catch (FileNotFoundException fnfe) {
            error++;
        }
        catch (Exception e) {
            error++;
            Log.e("tag", e.getMessage());
        }

    }


}

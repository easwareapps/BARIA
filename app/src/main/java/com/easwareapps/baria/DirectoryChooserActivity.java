/**
 ************************************** ॐ ***********************************
 ***************************** लोकाः समस्ताः सुखिनो भवन्तु॥**************************
 * <p/>
 * BARIA - Backup And Restore Installed Apps
 * Copyright (C) 2016  vishnu@easwareapps.com
 * <p/>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * <p/>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * <p/>
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 package com.easwareapps.baria;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.easwareapps.baria.adapter.DirectoryAdapter;
import com.easwareapps.baria.utils.BariaPref;


import java.io.File;
import java.util.ArrayList;

public class DirectoryChooserActivity extends AppCompatActivity {

    ArrayList<File> dirs;
    RecyclerView dirList;
    DirectoryAdapter directoryAdapter = null;
    int theme;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(savedInstanceState != null) {
            super.onCreate(savedInstanceState);
            return;
        }
        BariaPref pref = BariaPref.getInstance(getApplicationContext());
        theme = pref.getTheme(false);
        setTheme(theme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directory_chooser);
        dirList = (RecyclerView) findViewById(R.id.dirList);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        dirList.setLayoutManager(llm);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.mipmap.ic_back);
        setSupportActionBar(toolbar);
        if(toolbar != null) {
            toolbar.setTitle(getResources().getString(R.string.select_directory));
            toolbar.setTitleTextColor(Color.WHITE);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                goBack();
                }
            });

        }
        directoryAdapter = DirectoryAdapter.getInstance(getApplicationContext(), dirList, toolbar);



    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            //your code
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //your code

        }
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    private void goBack() {
        if (directoryAdapter.isAtRoot()) {
            setResult(-1, null);
            finish();
        }else {
            directoryAdapter.goBack();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.directory_selection_menu, menu);
        return true;
    }

    EditText txtDirName;
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.menu_select){

            Intent databackIntent = new Intent();
            databackIntent.putExtra("directory", directoryAdapter.getCurrentPath());
            setResult(0, databackIntent);
            finish();

        } else if(id == R.id.menu_up){
            if(!directoryAdapter.isAtRoot())
                directoryAdapter.goBack();
        } else if (id == R.id.menu_add) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getResources().getString(R.string.new_directory));

            builder.setView(R.layout.new_directory);

            builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    createNewDir();
                }
            });



            builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
            txtDirName = (EditText) dialog.findViewById(R.id.new_dir_name);
        }
        return super.onOptionsItemSelected(item);
    }

    private void createNewDir() {

        if(!directoryAdapter.addNewDirectory(
                txtDirName.getText().toString()).equals("true")) {

            Toast.makeText(getApplicationContext(),
                    getResources().getString(R.string.dir_creation_error), Toast.LENGTH_LONG).show();

        }
    }
}
